//1. Print "hello" 5 times
// use for loop
for(x = 0; x<5; x++){
    console.log("Hello!")
}
// use while loop
var x = 0
while(x<5){
    if(x >= 6){
        console.log(" stop! ")
        break;
    }
    console.log("Hello!")
    x = x+1;
}

// print odd number from 1 to 25
// use for looping
for (var x =1; x<= 25; x++){
    if( x % 2 === 1){
        console.log(x + "is odd number");
    }
}
//use while loop
var x = 1
while (x < 26){
    if( x % 2 === 0){
        break;
    }
    console.log(x + " is odd number");
    x = x+1;
}